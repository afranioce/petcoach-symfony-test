<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    protected $firstname;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    protected $lastname;

    /**
     * @ORM\ManyToOne(targetEntity="\UserBundle\Entity\Person", inversedBy="account", cascade={"persist"})
     */
    protected $person;

    /**
     * @return string
     */
    public function getName()
    {
        return !empty($this->firstname)
            ? trim("$this->firstname $this->lastname")
            : $this->username;
    }

    public function __toString()
    {
        return (string)$this->getName();
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set person
     *
     * @param \UserBundle\Entity\Person $person
     *
     * @return User
     */
    public function setPerson(\UserBundle\Entity\Person $person = null)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \UserBundle\Entity\Person
     */
    public function getPerson()
    {
        return $this->person;
    }
}
