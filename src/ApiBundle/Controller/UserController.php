<?php

namespace ApiBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Form\Factory\FactoryInterface;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use UserBundle\Entity\Student;
use UserBundle\Entity\Teacher;
use UserBundle\Entity\User;

/**
 * @Rest\RouteResource(pluralize=false)
 */
class UserController extends FOSRestController
{
    /**
     * Get profile info
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes={
     *     200 = "Returned when successful",
     *     403 = "Returned when access denied"
     *   }
     * )
     *
     * @return View
     */
    public function getMeAction()
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedHttpException('This user does not have access to this section.');
        }

        $view = new View($this->getUser());

        return $view;
    }

    /**
     * Edit my profile
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes={
     *     200 = "Returned when successful",
     *     403 = "Returned when access denied",
     *     400 = "Bad request"
     *   }
     * )
     *
     * @param Request $request
     * @return FormTypeInterface[]|View
     */
    public function putMeAction(Request $request)
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedHttpException('This user does not have access to this section.');
        }

        /** @var $formFactory FactoryInterface */
        $formFactory = $this->get('fos_user.profile.form.factory');

        $form = $formFactory->createForm(['csrf_protection' => false]);
        $form->setData($user);
        $this->processForm($request, $form);

        if ($form->isValid()) {
            /** @var $userManager UserManagerInterface */
            $userManager = $this->get('fos_user.user_manager');

            $userManager->updateUser($user);

            return new View($user);
        }

        return $form;
    }

    /**
     * Register user
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes={
     *     200 = "Returned when successful",
     *     403 = "Returned when access denied",
     *     400 = "Bad request"
     *   }
     * )
     *
     * @Rest\QueryParam(name="person", requirements="(student|teacher)", default="student", description="The Person type is student or teacher")
     *
     * @param Request $request
     * @param ParamFetcherInterface $paramFetcher
     * @return FormTypeInterface[]|View
     */
    public function postRegisterAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        /** @var \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');
        /** @var \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        /** @var User $user */
        $user = $userManager->createUser();

        switch ($paramFetcher->get('person')) {
            case 'student':
                $user->setPerson(new Student());
                $user->addRole('ROLE_STUDENT');
                break;
            case 'teacher':
                $user->setPerson(new Teacher());
                $user->addRole('ROLE_TEACHER');
                break;
        }

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm(['csrf_protection' => false]);
        $form->setData($user);
        $this->processForm($request, $form);

        if ($form->isValid()) {
            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);

            $userManager->updateUser($user);

            if ($this->getUser() instanceof UserInterface) {
                return new View($this->getUser());
            } else {
                return new View('A confirmation email has been sent to your email.', Response::HTTP_CREATED);
            }
        }

        return $form;
    }

    /**
     * @param  Request $request
     * @param  FormInterface $form
     */
    private function processForm(Request $request, FormInterface $form)
    {
        $data = json_decode($request->getContent(), true);

        if ($data === null) {
            throw new BadRequestHttpException('Data is empty.');
        }

        $form->submit($data);
    }
}
