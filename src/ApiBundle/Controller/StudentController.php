<?php

namespace ApiBundle\Controller;

use ApiBundle\Model\StudentCollection;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use UserBundle\Entity\Student;

/**
 * @Rest\RouteResource("Student")
 */
class StudentController extends FOSRestController
{
    /**
     * List all students
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     403 = "Returned when access denied"
     *   }
     * )
     *
     * @Rest\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing students.")
     * @Rest\QueryParam(name="limit", requirements="\d+", default="5", description="How many students to return.")
     *
     * @param ParamFetcherInterface $paramFetcher
     * @return StudentCollection
     */
    public function cgetAction(ParamFetcherInterface $paramFetcher)
    {
        if (!$this->isGranted('ROLE_TEACHER')) {
            throw $this->createAccessDeniedException();
        }

        $offset = $paramFetcher->get('offset');
        $start = null == $offset ? 0 : $offset + 1;
        $limit = $paramFetcher->get('limit');

        $students = $this->getDoctrine()->getRepository('UserBundle:Student')->fetch($start, $limit);

        return new StudentCollection($students);
    }

    /**
     * @ApiDoc(
     *   output = "UserBundle\Entity\Student",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     403 = "Returned when access denied",
     *     404 = "Returned when the is not found"
     *   }
     * )
     *
     * @param Student $student The student entity
     *
     * @return View
     */
    public function getAction(Student $student)
    {
        if (!$this->isGranted('ROLE_TEACHER')) {
            throw $this->createAccessDeniedException();
        }

        $view = new View($student);

        return $view;
    }

    /**
     * Removes a student.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes={
     *     204 = "Returned when successful",
     *     403 = "Returned when access denied",
     *     404 = "Returned when the is not found"
     *   }
     * )
     *
     * @param Student $student The student entity
     *
     * @return View
     */
    public function deleteAction(Student $student)
    {
        if (!$this->isGranted('ROLE_TEACHER')) {
            throw new AccessDeniedHttpException('Access denied');
        }

        $em = $this->getDoctrine()->getManager();

        $em->remove($student);
        $em->flush();

        $view = new View(null, Response::HTTP_NO_CONTENT);

        return $view;
    }
}
