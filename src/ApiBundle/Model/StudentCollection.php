<?php

namespace ApiBundle\Model;

use UserBundle\Entity\Student;

class StudentCollection
{
    /**
     * @var Student[]
     */
    public $data;

    /**
     * @var integer
     */
    public $offset;

    /**
     * @var integer
     */
    public $limit;

    /**
     * @param Student[] $students
     * @param integer $offset
     * @param integer $limit
     */
    public function __construct($students = [], $offset = null, $limit = null)
    {
        $this->data = $students;
        $this->offset = $offset;
        $this->limit = $limit;
    }
}
