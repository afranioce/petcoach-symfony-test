# Instalação

Caso não tenha o docker instalado vá para o passo _2_

#### 1 - Usando docker compose *Opcional*

```bash
docker-composer up
```

Acessar a imagem docker

```bash
docker exec -it test_php_1 bash
```

#### 2 - Instalação do projeto

```bash
composer install
```

#### 3 - Criar client Oauth2

```bash
php app/console fos:oauth-server:create-client
```

### Testes

Necessário trocar o `client_id` e `client_server` criado no passo 3 

```bash
bin/phpunit -c app/ tests
```
