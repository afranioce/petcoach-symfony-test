<?php

namespace Tests\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class StudentControllerTest extends WebTestCase
{
    public function testGetStudens()
    {
        $client = $this->getClient(true);

        $client->request('GET', 'api/students.json');
        $response = $client->getResponse();

        $content = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertArrayHasKey('data', (array)$content);
    }

    public function testGetStudent()
    {
        $client = $this->getClient(true);
        $client->request('GET', 'api/students/0.json');
        $response = $client->getResponse();
        $this->assertEquals(404, $response->getStatusCode());
    }

    public function testDeleteStudent()
    {
        $client = $this->getClient(true);

        $client->request('DELETE', 'api/students/0.json');
        $response = $client->getResponse();

        $this->assertEquals('{"code":404,"message":"UserBundle\\\\Entity\\\\Student object not found."}',$response->getContent());
    }

    private function getClient($authenticated = false)
    {
        $params = [];

        $client = static::createClient();

        if ($authenticated) {
            $params = array_merge($params, [
                'grant_type' => 'password',
                'client_id' => '8_5ass39yii30occwowk88w8c84sckocwooccw8808sc0sw4wcok',
                'client_secret' => '5561px8elocgsgocw00gc048k408404g0ws0gsw4sosog08444',
                'username' => 'student2',
                'password' => '123'
            ]);

            $client->request('POST', 'oauth/v2/token', $params);
            $response = $client->getResponse();

            $content = json_decode($response->getContent());

            $this->assertEquals(200, $response->getStatusCode());

            $client->restart();

            $client->setServerParameters([
                'HTTP_AUTHORIZATION' => "Bearer {$content->access_token}"
            ]);
        }

        return $client;
    }
}
