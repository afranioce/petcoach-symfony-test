<?php

namespace Tests\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MeControllerTest extends WebTestCase
{
    public function testGetMe()
    {
        $client = $this->getClient(true);

        $client->request('GET', 'api/me.json');
        $response = $client->getResponse();
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
    }

    public function testPutMe()
    {
        $client = $this->getClient(true);
        $content = [
            'username' => 'usertupdatetest',
            'email' => 'usertupdatetest@email.com',
            'current_password' => 123
        ];

        $client->request('PUT', 'api/me.json', [], [], [], json_encode($content));
        $response = $client->getResponse();
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $this->assertEquals('{"id":1,"email":"usertupdatetest@email.com"}', $response->getContent());
    }

    public function testRegisterStudent()
    {
        $client = $this->getClient(false);

        $content = [
            'username' => 'studenttest',
            'email' => 'studenttest@email.com',
            'plainPassword' => [
                'first' => 123,
                'second' => 123
            ]
        ];

        $client->request('POST', '/api/register.json', [], [], [], json_encode($content));
        $response = $client->getResponse();

        $this->assertEquals(201, $response->getStatusCode(), $response->getContent());
        $this->assertEquals('"A confirmation email has been sent to your email."', $response->getContent());
    }

    public function testRegisterTeacher()
    {
        $client = $this->getClient(false);

        $content = [
            'username' => 'teachertest',
            'email' => 'teachertest@email.com',
            'plainPassword' => [
                'first' => 123,
                'second' => 123
            ]
        ];

        $client->request('POST', '/api/register.json?person=teacher', [], [], [], json_encode($content));

        $response = $client->getResponse();

        $this->assertEquals(201, $response->getStatusCode(), $response->getContent());
        $this->assertEquals('"A confirmation email has been sent to your email."', $response->getContent());
    }

    private function getClient($authenticated = false)
    {
        $params = [];

        $client = static::createClient();

        if ($authenticated) {
            $params = array_merge($params, [
                'grant_type' => 'password',
                'client_id' => '8_5ass39yii30occwowk88w8c84sckocwooccw8808sc0sw4wcok',
                'client_secret' => '5561px8elocgsgocw00gc048k408404g0ws0gsw4sosog08444',
                'username' => 'teste',
                'password' => '123'
            ]);

            $client->request('POST', 'oauth/v2/token', $params);
            $response = $client->getResponse();

            $content = json_decode($response->getContent());

            $this->assertEquals(200, $response->getStatusCode());

            $client->restart();

            $client->setServerParameters([
                'HTTP_AUTHORIZATION' => "Bearer {$content->access_token}",
                'CONTENT_TYPE' => 'application/json'
            ]);
        }

        return $client;
    }
}
